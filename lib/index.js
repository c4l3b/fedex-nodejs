var soap = require('soap');
var extend = require('extend');
var path = require('path');

function FedEx(args) {
    var $scope = this;

    // config defaults
    var defaults = {
        key: '',
        password: '',
        account_number: '',
        meter_number: ''
    }

    // these are our accessible endpoints
    var endpoints = {
        ship: {
            f:       'processShipment',
            r:       handleBaseResponse,
            wsdl:    'ShipService_v21.wsdl',
            version: { ServiceId: 'ship', Major: 21, Intermediate: 0, Minor: 0 }
        },
        addressValidation: {
            f:       'addressValidation',
            r:       handleBaseResponse,
            wsdl:    'AddressValidationService_v4.wsdl',
            version: { ServiceId: 'aval', Major: 4, Intermediate: 0, Minor: 0 }
        }
    }

    // init and set our configs
    $scope.init = function(args) {
        $scope.options = extend(defaults, args);
        return $scope;
    }

    // build out the auth params
   function buildAuthentication(resource) {
        return params = {
            WebAuthenticationDetail: {
                UserCredential: {
                    Key:        $scope.options.key,
                    Password:   $scope.options.password
                }
            },
            ClientDetail: {
                AccountNumber:  $scope.options.account_number,
                MeterNumber:    $scope.options.meter_number
            },
            Version: {
                ServiceId:      resource.version.ServiceId,
                Major:          resource.version.Major,
                Intermediate:   resource.version.Intermediate,
                Minor:          resource.version.Minor
            }
        }
    }

    // build a new request
    function buildBaseRequest(data, resource, callback, method) {
        soap.createClient(path.join(__dirname, 'wsdl', resource.wsdl), function(err, client) {
            if (err) {
                return callback(err, null);
            }

            var params = buildAuthentication(resource);

            params = extend(params, data);

            client[method](params, function(err, result) {
                if (err) {
                    return callback(err, null);
                }

                return callback(null, result);
            });
        });
    }

    // handle request response
    function handleBaseResponse(res, callback) {
        return callback(null, res);
    }

    // turn our endpoints into accessible functions
    function buildEndpointFunction(fn) {
        return function(data, callback) {
            var requestCallback = function (err, res) {
                if (err) {
                    return callback(err, null);
                }
                endpoints[fn].r(res, callback);
            };

            if (typeof endpoints[fn].f === 'string') {
                buildBaseRequest(data, endpoints[fn], requestCallback, endpoints[fn].f);
            } else {
                endpoints[fn].f(data, endpoints[fn], requestCallback);
            }
        }
    }

    for (var fn in endpoints) {
        $scope[fn] = buildEndpointFunction(fn);
    }

    return $scope.init(args);
}

module.exports = FedEx;